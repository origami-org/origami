# origami

A balanced shadowsocks + v2ray-plugin client keep in system tray.

origami integrates custom [go-shadowsocks2][] and [v2ray-plugin][]. It's able to,

* Out of the box by a preset gateway.
* Use Cloudflare CDN to balance outgoing by default.
* Set to use your own gateway.
* Set to use non Cloudflare CDN.

[go-shadowsocks2]: https://gitlab.com/origami-org/go-shadowsocks2/
[v2ray-plugin]: https://gitlab.com/origami-org/v2ray-plugin/

> origami - 折り紙
>
> Google Translate:
> The Japanese art of folding paper into decorative shapes and figures.

## Install

### Linux Install

You can install origami by command,

```sh
# NOT IMPLEMENTED:
#
# The following command is not work at all, for now,
# cause the gitlab pages are not implemented.
#
# wget -O- https://origami-org.gitlab.io/origami/install.sh | bash
```

Or, by `go` toolchains.

```sh
# Install required packages
sudo apt-get install --no-install-recommends libgtk-3-dev libappindicator3-dev
# Install origami
go get -u gitlab.com/origami-org/origami
```

### Mac Install

«TODO»

### Windows Install

«TODO»

## Config (optional)

origami is preset to work out of the box. But it still able to configured to work with your own gateway. The user own gateway can be any implementation of shadowsocks which support v2ray-plugin.

1. shadowsocks

    «TODO»

2. v2ray-plugin

    «TODO»

3. CDN

    «TODO»

4. origami config file

    «TODO»
