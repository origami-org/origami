package main

import "net"

// Dear developers,
//
// The func GetFreePort() is separated a .go file, because of the conflicted net packages.
//
// - main.go has net package "v2ray.com/core/common/net"
// - getfreeport.go has net package "net"
//
// Separated .go files avoid meanless package renaming, e.g.,
//
//     import sysnet "net"
//     import v2raynet "v2ray.com/core/common/net"

// GetFreePort asks the kernel for a free open port that is ready to use.
func GetFreePort() (int, error) {
	addr, err := net.ResolveTCPAddr("tcp", "localhost:0")
	if err != nil {
		return 0, err
	}

	l, err := net.ListenTCP("tcp", addr)
	if err != nil {
		return 0, err
	}
	defer l.Close()
	return l.Addr().(*net.TCPAddr).Port, nil
}
