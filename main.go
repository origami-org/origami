package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"strings"
	"time"

	"gopkg.in/yaml.v2"

	"v2ray.com/core/common/net"

	"github.com/getlantern/systray"
	"github.com/getlantern/systray/example/icon"

	shadowsocks "gitlab.com/origami-org/go-shadowsocks2/client"
	"gitlab.com/origami-org/go-shadowsocks2/core"
	v2ray "gitlab.com/origami-org/v2ray-plugin/client"
)

var defaultRemoteAddrs = []string{"104.27.181.1", "104.27.181.2", "104.27.181.3", "104.27.181.4", "104.27.181.5", "104.27.181.6", "104.27.181.7", "104.27.181.8"}

const (
	defaultRemotePort net.Port = 443
	defaultHost                = "shrimpy.encom.eu.org"
	defaultPath                = "/"
	defaultMux                 = 1
	defaultCipher              = "AEAD_AES_256_GCM"
	defaultPassword            = "chacha20-ietf-poly1305"
	defaultLocalPort  net.Port = 29083
)

type Controller struct {
	RemoteAddrs []string `yaml:"Remote Addrs" json:"remote_addrs"`
	RemotePort  net.Port `yaml:"Remote Port"  json:"remote_port"`
	Host        string   `yaml:"Host"         json:"host"`
	Path        string   `yaml:"Path"         json:"path"`
	Mux         uint     `yaml:"Mux"          json:"mux"`
	Cipher      string   `yaml:"Cipher"       json:"cipher"`
	Password    string   `yaml:"Password"     json:"password"`
	LocalPort   net.Port `yaml:"Local Port"   json:"local_port"`

	isRunning   bool
	isConnected bool

	configFilepath string

	shadowsocksClient *shadowsocks.ShadowsocksClient
	v2rayClient       *v2ray.V2rayClient
}

// NewController creates a Controller with values initialized.
// It tries to initial values from config file if the file is exist.
// Otherwise it initials values with defaults.
func NewController() *Controller {
	var c = Controller{configFilepath: tryConfigFile()}
	c.ReloadConfig()
	return &c
}

// ReloadConfig reloads configurations.
// It tries to load values from config file if the file is exist.
// Otherwise it loads values from defaults.
func (c *Controller) ReloadConfig() {
	// No config file found, use defaults
	if c.configFilepath == "" {
		c.RemoteAddrs = defaultRemoteAddrs
		c.RemotePort = defaultRemotePort
		c.Host = defaultHost
		c.Path = defaultPath
		c.Mux = defaultMux
		c.Cipher = defaultCipher
		c.Password = defaultPassword
		c.LocalPort = defaultLocalPort
		return
	}

	// Read config file
	b, err := ioutil.ReadFile(c.configFilepath)
	if err != nil {
		log.Fatal(err)
	}

	if strings.HasSuffix(c.configFilepath, ".yaml") {
		if err = yaml.Unmarshal(b, &c); err != nil {
			log.Fatal(err)
		}
	} else {
		if err = json.Unmarshal(b, &c); err != nil {
			log.Fatal(err)
		}
	}

	// Feed empties with defaults
	if c.RemoteAddrs == nil {
		c.RemoteAddrs = defaultRemoteAddrs
	}
	if c.RemotePort == 0 {
		c.RemotePort = defaultRemotePort
	}
	if c.Host == "" {
		c.Host = defaultHost
	}
	if c.Path == "" {
		c.Path = defaultPath
	}
	if c.Mux == 0 {
		c.Mux = defaultMux
	}
	if c.Cipher == "" {
		c.Cipher = defaultCipher
	}
	if c.Password == "" {
		c.Password = defaultPassword
	}
	if c.LocalPort == 0 {
		c.LocalPort = defaultLocalPort
	}

	if !isValideCipher(c.Cipher) {
		ciphers := strings.Join(core.ListCipher(), ", ")
		log.Fatalf("Bad cipher: %s (valid ciphers: %s)", c.Cipher, ciphers)
	}
}

func (c *Controller) Connect() {
	if c.isConnected {
		return
	}

	loopbackPort, err := GetFreePort()
	if err != nil {
		log.Fatal(err)
	}

	var remoteConfigs []v2ray.ServerConfig

	for _, remoteAddr := range c.RemoteAddrs {
		remoteConfigs = append(remoteConfigs, v2ray.ServerConfig{
			Address: remoteAddr,
			Port:    c.RemotePort,
		})
	}

	c.v2rayClient = &v2ray.V2rayClient{
		Local: v2ray.ServerConfig{
			Address: "127.0.0.1",
			Port:    net.Port(loopbackPort),
		},
		Remote: remoteConfigs,

		Mux:  c.Mux,
		Host: c.Host,
		Path: c.Path,
	}

	c.v2rayClient.Start()

	c.shadowsocksClient = &shadowsocks.ShadowsocksClient{
		Socks:    fmt.Sprintf("127.0.0.1:%d", c.LocalPort),
		Address:  fmt.Sprintf("127.0.0.1:%d", loopbackPort),
		Cipher:   c.Cipher,
		Password: c.Password,
	}

	c.shadowsocksClient.Start()

	c.isConnected = true
}

func (c *Controller) Disconnect() {
	if !c.isConnected {
		return
	}

	c.v2rayClient.Stop()
	c.shadowsocksClient.Stop()

	c.isConnected = false
}

// Reconnect recreates connection with the recent configurations.
func (c *Controller) Reconnect() {
	if c.isConnected {
		c.Disconnect()
	}
	time.Sleep(time.Second)
	c.ReloadConfig()
	c.Connect()
}

func init() {
	os.Setenv("GODEBUG", os.Getenv("GODEBUG")+",tls13=1")
}

func main() {
	c := NewController()

	c.Connect()

	systray.Run(func() { onReady(c) }, func() { onExit(c) })
}

func onReady(c *Controller) {
	systray.SetIcon(icon.Data)
	systray.SetTitle("Origami")
	systray.SetTooltip(fmt.Sprintf("Local address: 127.0.0.1:%d", c.LocalPort))

	mReconnect := systray.AddMenuItem("Reconnect", "Reconnect proxy")
	systray.AddSeparator()
	mQuit := systray.AddMenuItem("Quit", "Quit the whole app")

	go func() {
		for {
			select {
			case <-mReconnect.ClickedCh:
				systray.SetTooltip("Reconnecting")
				c.Reconnect()
				systray.SetTooltip(fmt.Sprintf("Local address: 127.0.0.1:%d", c.LocalPort))
			case <-mQuit.ClickedCh:
				systray.Quit()
				return
			}
		}
	}()
}

func onExit(c *Controller) {
	c.Disconnect()
}

// tryConfigFile returns the full path of config file when there found a
// config file at origami binary file directory, or empty string on not.
func tryConfigFile() string {
	path, err := os.Executable()
	if err != nil {
		log.Fatal(err)
	}

	dir := filepath.Dir(path)

	_, err = os.Stat(dir + "/origami.yaml")
	if !os.IsNotExist(err) {
		return dir + "/origami.yaml"
	}

	_, err = os.Stat(dir + "/origami.json")
	if !os.IsNotExist(err) {
		return dir + "/origami.json"
	}

	return ""
}

// isValideCipher returns true when the name is a valide cipher, false on not.
func isValideCipher(name string) bool {
	for _, v := range core.ListCipher() {
		if name == v {
			return true
		}
	}
	return false
}
